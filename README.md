
# Feefo: Technical Assessment

In this repo you will find:

- Code for the UI Technical assessment
- Answers (diagrams + markdown doc) for the RESTful API design assessment


## We would like to see
- The layout should grow/shrink sensibly with the viewport *(styled-media-query has been added as an npm package, and the page should be compatible for device widths above 320px)*
- The design split into several components *(see src/components. Note that this is a first cut of creating a component library and would be better defined following discussions with UI/UX designers)*
- Usage of `proptypes` *(TypeScript interfaces would be preferred, but see e.g. src/pages/account-overview.jsx)*
- Unit tests covering your components *(see the `Testing considerations` section below, but also src/pages/account-overview.test.js and src/components/infographic/infographic.test.js)*
- a11y considered and relevant ARIA attributes added *(some semantic HTML used (e.g. `section` / `main`), as well as heading levels, `aria-role`, and `aria-describedby` attributes)* 


## General code comments

- I'd ordinarily set up projects with an i18n library to avoid having any inline text and to future-proof for multi-language support
- The eslint config in `package.json` seems a bit thin; I'd ordinarily have a bit more of a comprehensive `.eslint` file
- `@testing-library/*` should be in `devDependencies`
- TypeScript would be much preffered over vanilla JS
- '.env' files would be useful to feature test new parts of the application
- It would be useful to have a way of visualising our component library without spinning up the application (e.g. through Storybook)
- A wrapper around the FontAwesome library would be useful so that the icon library is abstracted away and can easily be switched out for another library


## Testing considerations

- Some form of visual regression testing would be useful for our component library
- Some form of a 'smoke test' integration suite (e.g. Selenium / Puppeteer / a test automation SaaS) would be useful for more complicated 'flows'
- As the application grows, utilities / business logic ought to be factored out of the React components and unit tested



## Setup
1. Run `npm install` to install required dependencies
2. Run `npm run start` to start a local development server

