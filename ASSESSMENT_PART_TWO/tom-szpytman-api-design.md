# Web App RESTful API design

## Data Model

The current application consists of a simple feature set, but is expected to grow in features over the coming years. The
exact requirements for future features are, at present, unknown. Given that our data can be easily modelled in a
relational way, I suggest that we start with a Relational database schema (it is much easier to move from a Relational
-> Non-Relational database, than vice versa).

We have two main entities; *Users* and *Notes*. These could be described by the following Data Model:

```typescript
interface User {
  id: number // Primary key, auto increment Integer field in the DB
  dateCreated: string // This would be stored as a DateTime (with TZ) in the DB
  isActive: boolean // It is likely this would just be stored as `active` (rather than isActive) in the database and default to `true`
  dateDeactivated: string | null // This would be stored as a DateTime (with TZ) in the DB
  username: string // We would probably add min & max length constraints, only allow certain characters, and enforce a global uniqueness constraint. Likely we'd store this as a VARCHAR field in the DB
  password: string // This would be hashed + salted before being saved into the DB and wouldn't ever be re-exposed to the user. Likely we'd store this as a VARCHAR field in the DB
}
```

```typescript
interface Note {
  id: number // Primary key, auto increment Integer field in the DB
  dateCreated: string // This would be stored as a DateTime (with TZ) in the DB
  isActive: boolean // It is likely this would just be stored as `active` (rather than isActive) in the database and default to `true`
  dateDeactivated: string | null // This would be stored as a DateTime (with TZ) in the DB
  content: string // We may add min & max length constraints and only allow certain characters. Likely we'd store this as a TEXT field in the DB
  userId: number // Foreign key constraint which references the `User` table and the column `id`
}
```

Notes:

* The foreign key constraint would allow a Many-To-One mapping of Notes to a User
* Rather than 'hard deleting' Notes and Users, the `isActive` flag would allow us to fulfill the requirements of '
  deleting' notes, but would do so without us having to remove a row of data from the database

## RESTful API

We need ways to:

* Retrieve a given User's notes
* Create a new note

To retrieve a given User's notes, we could set up a `GET` endpoint with either the paths:

* `/user/notes` (if Notes are private and only available to their authors)
* `/users/<userId>/notes` (if Notes are to be globally accessible, e.g. if we were to allow note sharing)

The endpoint would return an array of `Note` objects, along with a 200 status code.

To create a new note, we could set up a `POST` endpoint which could exist on the same path (`/users/<userId>/notes`
or `/user/notes`).

The `POST` endpoint would accept the following as the body of the request:

```typescript
interface POSTBody {
  content: string
}
```

From the context of the request, the API controller would work out the `userId`, attach this to the Database request,
and let the Database handle adding in the values for `id`, `dateCreated` and `isActive` fields. If successful, the
endpoint would then return a `Note` object along with a 200 status code.

As a minimum, both endpoints would be programmed to return the following status codes in exceptional circumstances:

* 401: If the request is unauthorised / does not have the correct permissions
* 500: If a server error has occurred
* 400: If the user's request is considered 'bad' or malformed in some way

### Web Server

* The Web Server would contain a series of controllers for each RESTful API endpoint (note; there would be one
  controller per path, and the controller would handle the different HTTP methods)
* There may be some middleware that first checks whether a request is authenticated / authorised and would respond with
  a 401 if the checks fail. If a request passes these checks, it would then be passed on to the controller
* When reading out data from the database, the application's database layer would map the rows of data onto an array of
  pre-defined Data Model objects (e.g. `Note`) and marshall the notes around as, e.g. a `Note` array. The same
  marshalling process would take place when an incoming request body is mapped to a pre-defined Object
* The business logic of validating the submitted form would take place in the Controller (as well as in the Client
  application)