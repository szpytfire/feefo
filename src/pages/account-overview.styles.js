import styled from 'styled-components';
import {COLOUR} from "../components/colours";
import {UNIT_2, UNIT_4} from "../components/grid";
import media from 'styled-media-query'

export const AccountOverviewWrapper = styled.main`
  background-color: ${COLOUR.greys.slate40};
  padding: ${UNIT_2} ${UNIT_4};
`

export const SummaryHeader = styled.div`
  display: flex;
  flex-direction: column;

  ${media.greaterThan('medium')`
    flex-direction: row;
    justify-content: space-between;
  `}
  
  margin: ${UNIT_4} 0;
`