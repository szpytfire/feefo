import React from 'react';
import PropTypes from 'prop-types';

import {SalesOverview} from "../components/sales-overview/sales-overview";
import {AccountOverviewWrapper, SummaryHeader} from "./account-overview.styles";
import {H1} from "../components/typography";
import {ContactSummary} from "../components/contact-summary/contact-summary";

export const AccountOverview = ({data}) => {
    return (
        <AccountOverviewWrapper>
            <SummaryHeader>
                <H1>Account Overview</H1>
                <ContactSummary {...data.supportContact} />
            </SummaryHeader>
            <SalesOverview {...data.salesOverview} />
        </AccountOverviewWrapper>
    )
}

AccountOverview.propTypes = {
    supportContact: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
        initials: PropTypes.string,
        phoneNumber: PropTypes.string,
    }),
    salesOverview: PropTypes.shape({
        uploads: PropTypes.number,
        successfulUploads: PropTypes.number,
        linesAttempted: PropTypes.number,
        linesSaved: PropTypes.number,
        lastUploadDate: PropTypes.number,
    }),
}
