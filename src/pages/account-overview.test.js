import {render, screen} from '@testing-library/react';
import {AccountOverview} from './account-overview';
import {MOCK_DATA} from "../mock-data";

const checkHeaderExists = (level) => {
    render(<AccountOverview data={MOCK_DATA}/>);
    const element = screen.getByRole('heading', {level});
    expect(element).toBeInTheDocument();
}

test('renders h1 component', () => {
    checkHeaderExists(1)
});

test('renders h2 component', () => {
    checkHeaderExists(2)
});

test('renders h3 component', () => {
    checkHeaderExists(3)
});

test('renders h4 components', () => {
    render(<AccountOverview data={MOCK_DATA}/>);
    const elements = screen.getAllByRole('heading', {level: 4});
    expect(elements.length).toEqual(2);
});

test('renders h5 component', () => {
    checkHeaderExists(5)
});
