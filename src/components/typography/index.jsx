import styled from 'styled-components';
import {COLOUR} from "../colours";
import {UNIT_1} from "../grid";

export const H1 = styled.h1`
  color: ${COLOUR.greys.slate100};
  font-weight: 400;
`

export const H2 = styled.h2`
  font-size: 1em;
  font-weight: 700;
  text-transform: uppercase;
  color: ${COLOUR.greys.slate80};
  margin: ${UNIT_1} 0;
`

export const H3 = styled.h3`
`

export const H4 = styled.h4`
  color: ${COLOUR.greys.slate80};
  font-weight: 700;
  text-transform: uppercase;
  font-size: 0.9em;
  margin: ${UNIT_1} 0;
`

export const H5 = styled.h5`
  color: ${COLOUR.greys.slate100};
  margin: 0;
`

export const Figure = styled.p`
  color: ${COLOUR.primary};
  font-weight: bold;
  font-size: 3em;
  padding: 0;
  margin: 0;
  line-height: 0.9em;
`

export const Caption = styled.p`
  color: ${COLOUR.greys.slate100};
`