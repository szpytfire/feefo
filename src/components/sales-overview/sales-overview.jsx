import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faInfoCircle, faUpload} from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import {Infographics, SalesOverviewWrapper, SalesSummary, SalesSummaryHeader} from "./sales-overview.styles";
import {Infographic} from "../infographic/infographic";
import {Caption, H3} from "../typography";
import {COLOUR} from "../colours";

export const SalesOverview = ({uploads, successfulUploads, linesAttempted, linesSaved}) => {
    // TODO: consider factoring out <SalesSummary>...</SalesSummary> into its own component
    return (
        <SalesOverviewWrapper>
            <SalesSummary>
                <SalesSummaryHeader>
                    <FontAwesomeIcon icon={faUpload} color={COLOUR.tertiary} title='Sales Summary Icon'/>
                    <H3>Sales</H3>
                    <FontAwesomeIcon icon={faInfoCircle} color={COLOUR.greys.slate80} title='Sales More Info'
                                     role="tooltip"/>
                </SalesSummaryHeader>
                <Caption>You had <b>{uploads} uploads</b> and <b>{linesAttempted}</b> lines added.</Caption>
            </SalesSummary>
            <Infographics>
                <Infographic label={'Upload Success'} percentage={successfulUploads}/>
                <Infographic label={'Lines Saved'} percentage={linesSaved}/>
            </Infographics>
        </SalesOverviewWrapper>
    )
}

SalesOverview.propTypes = {
    uploads: PropTypes.number,
    successfulUploads: PropTypes.number,
    linesAttempted: PropTypes.number,
    linesSaved: PropTypes.number,
}
