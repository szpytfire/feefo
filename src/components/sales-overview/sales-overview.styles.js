import {COLOUR} from "../colours";
import styled from 'styled-components';
import {UNIT_0_25, UNIT_1, UNIT_2, UNIT_4} from "../grid";
import media from "styled-media-query";


export const SalesOverviewWrapper = styled.section`
  display: flex;
  flex-direction: column;
`

export const Infographics = styled.div`
  display: flex;
  flex-direction: column;

  > div:not(:last-of-type) {
    margin-bottom: ${UNIT_0_25};
  }

  ${media.greaterThan('medium')`
    flex-direction: row;
    justify-content: space-between;
  
    > div:not(:last-of-type) {
      margin-bottom: 0;
      margin-right: ${UNIT_0_25};
    }
  `}
`

export const SalesSummary = styled.div`
  background-color: ${COLOUR.greys.white};
  border-radius: ${UNIT_1};
  margin-bottom: ${UNIT_0_25};
  padding: ${UNIT_2} ${UNIT_4};
  display: flex;
  flex-direction: column;
`

export const SalesSummaryHeader = styled.div`
  display: flex;
  align-items: center;

  h3 {
    margin-left: ${UNIT_1};
    margin-right: auto;
  }
`