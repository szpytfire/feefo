import React from 'react';
import PropTypes from 'prop-types';
import {Contact, ContactDetails, ContactOverview, ContactWrapper, Initials} from "./contact-summary.styles";
import {Caption, H2, H5} from "../typography";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";
import {COLOUR} from "../colours";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const ContactSummary = ({name, email, initials, phoneNumber}) => {
    // TODO: Consider factoring out the <ContactOverview>...</ContactOverview> into its own component - pending discussion with designers
    return (
        <ContactWrapper>
            <H2>Your Feefo Support Contact</H2>
            <ContactOverview>
                <Initials aria-describedby="contact-name">{initials}</Initials>
                <Contact>
                    <H5 id="contact-name">{name}</H5>
                    <ContactDetails>
                        <FontAwesomeIcon icon={faEnvelope} color={COLOUR.greys.slate100}
                                         aria-describedby="contact-email-address"/>
                        <Caption id="contact-email-address">{email}</Caption>
                        <Caption>{phoneNumber}</Caption>
                    </ContactDetails>
                </Contact>
            </ContactOverview>
        </ContactWrapper>
    )
}

ContactSummary.propTypes = {
    name: PropTypes.string,
    email: PropTypes.string,
    initials: PropTypes.string,
    phoneNumber: PropTypes.string,
}
