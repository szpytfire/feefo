import {COLOUR} from "../colours";
import {UNIT_0_25, UNIT_1, UNIT_2} from "../grid";
import styled from 'styled-components';
import media from "styled-media-query";

export const ContactWrapper = styled.section`
  display: flex;
  flex-direction: column;
`

export const ContactOverview = styled.div`
  display: flex;
`

export const Contact = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: ${UNIT_1};
`

export const ContactDetails = styled.div`
  display: flex;
  align-items: flex-start;
  flex-direction: column;

  ${media.greaterThan('small')`
    align-items: center;
    flex-direction: row;
    
    > p {
      margin-left: ${UNIT_1};
    }
  `}
  > p {
    margin-top: ${UNIT_0_25};
    margin-bottom: ${UNIT_0_25};
  }
`

export const Initials = styled.div`
  background-color: ${COLOUR.secondary};
  border-radius: ${UNIT_1};
  padding: ${UNIT_2};
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: 800;
  color: ${COLOUR.greys.slate100};
  align-self: center;
`