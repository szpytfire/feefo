export const COLOUR = {
    primary: '#21ab55',
    secondary: '#f9cf02',
    tertiary: '#3eb1eb',
    greys: {
        slate100: '#484446',
        slate80: '#9b9898',
        slate40: '#f4f4f4',
        white: '#fff',
    }
}