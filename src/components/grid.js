const BASE_UNIT = 8
export const UNIT_0_25 = `${BASE_UNIT /4}px`
export const UNIT_1 = `${BASE_UNIT}px`
export const UNIT_2 = `${BASE_UNIT * 2}px`
export const UNIT_4 = `${BASE_UNIT * 4}px`
