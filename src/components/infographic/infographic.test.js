import {render, screen} from '@testing-library/react';
import {Infographic} from "./infographic";

test('sets up aria labelled by correctly', () => {
    const percentage = 57
    const label = 'My_Percentage'
    const expectedId = 'My_Percentage--57'

    render(<Infographic percentage={percentage} label={label}/>);

    // Check that the header has the correct id
    const header = screen.getByRole('heading', {level: 4});
    expect(header.id).toEqual(expectedId);

    // Check that the figure is labelled by the correct heading
    const figure = screen.getByText(`${percentage}%`);
    expect(figure.getAttribute('aria-labelledby')).toEqual(expectedId);
});

