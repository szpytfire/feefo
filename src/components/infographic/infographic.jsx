import React from 'react';
import PropTypes from 'prop-types';
import {InfographicWrapper} from "./infographic.styles";
import {Figure, H4} from "../typography";

export const Infographic = ({percentage, label}) => {
    // TODO: create a better id generator (and factor out into a utility); this id may not be globally unique
    const infographicId = React.useMemo(() => `${label}--${percentage}`, [percentage, label]);

    return (
        <InfographicWrapper>
            <Figure aria-labelledby={infographicId}>{percentage}%</Figure>
            <H4 id={infographicId}>{label}</H4>
        </InfographicWrapper>
    )
}

Infographic.propTypes = {
    percentage: PropTypes.number,
    label: PropTypes.string,
}
