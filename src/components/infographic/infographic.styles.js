import {COLOUR} from "../colours";
import styled from 'styled-components';
import {UNIT_1, UNIT_2, UNIT_4} from "../grid";

export const InfographicWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${COLOUR.greys.white};
  border-radius: ${UNIT_1};
  padding: ${UNIT_2} ${UNIT_4};
  flex: 1;
`