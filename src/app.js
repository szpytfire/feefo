import './app.css';
import {AccountOverview} from './pages/account-overview';
import {MOCK_DATA} from "./mock-data";

export const App = () => {
    return (
        <AccountOverview data={MOCK_DATA}/>
    );
}
